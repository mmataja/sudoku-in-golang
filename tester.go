package main

import (
	"fmt"
	"time"
)

type row []int
type matrix []row

var m = matrix{
	{5, 3, 0, 0, 7, 0, 0, 0, 0}, {6, 0, 0, 1, 9, 5, 0, 0, 0}, {0, 9, 8, 0, 0, 0, 0, 6, 0},
	{8, 0, 0, 0, 6, 0, 0, 0, 3}, {4, 0, 0, 8, 0, 3, 0, 0, 1}, {7, 0, 0, 0, 2, 0, 0, 0, 6},
	{0, 6, 0, 0, 0, 0, 2, 8, 0}, {0, 0, 0, 4, 1, 9, 0, 0, 5}, {0, 0, 0, 0, 8, 0, 0, 7, 9},
}

var poznateVrijednosti = make(chan int, 9)

func main() {

	/*for x := 0; x < 9; x++ {
		for y := 0; y < 9; y++ {
			fmt.Println("X =", x, "Y =", y, "Vrijednost na toj poziciji je:", m[x][y], "\n")
		}
	}*/

	fmt.Printf("\n")
	printBoard(m)

	go_rows(m)
	go_columns(m)
	time.Sleep(50 * time.Millisecond)

	for i := 0; i < cap(poznateVrijednosti)+1; i++ {
		val := <-poznateVrijednosti
		fmt.Printf("Vrijednosti iz kanala: %v\n", val)
	}
}

func transpose(m matrix) matrix {
	r := make(matrix, len(m[0]))
	for x, _ := range r {
		r[x] = make(row, len(m))
	}
	for y, s := range m {
		for x, e := range s {
			r[x][y] = e
		}
	}
	return r
}

func printBoard(board matrix) {
	fmt.Printf("=====================\n")
	for y := 0; y < 9; y++ {
		for x := 0; x < 9; x++ {
			fmt.Printf("%d ", board[y][x])
			if x%3 == 2 && x < 8 {
				fmt.Printf("| ")
			}
		}
		fmt.Printf("\n")
		if y%3 == 2 && y < 8 {
			fmt.Printf("-----   -----   -----\n")
		}
	}
	fmt.Printf("=====================\n\n")
}

func go_rows(rows matrix) {
	for i := 0; i < 9; i++ {
		fmt.Printf("Iteration: %v ->", i)
		fmt.Printf("%v\n", rows[i])
		go rijesi(rows[i], "r", i)
		time.Sleep(5 * time.Millisecond)
	}
}

func go_columns(columns matrix) {
	fmt.Printf("\n")
	t := transpose(columns)
	for i := 0; i < 9; i++ {
		fmt.Printf("Iteration: %v ->", i)
		fmt.Printf("%v\n", t[i])
		go rijesi(t[i], "r", i)
		time.Sleep(5 * time.Millisecond)
	}
}

func rijesi(redak []int, rs string, xy int) {
	brojRetkaStupca := make(chan int)
	for i := 0; i < 9; i++ {
		if redak[i] != 0 {
			poznateVrijednosti <- redak[i]
			brojRetkaStupca <- xy
		}
		//fmt.Printf("Vrijednost = %v\n", redak[i])
	}

	//fmt.Printf("%v\n", redak)
	//fmt.Printf("Broj redka/stupca = %v\n", xy)
}
