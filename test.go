package main

import (
	"fmt"
	"time"
)

type row []int
type matrix []row

var m = matrix{
	{5, 3, 0, 0, 7, 0, 0, 0, 0}, {6, 0, 0, 1, 9, 5, 0, 0, 0}, {0, 9, 8, 0, 0, 0, 0, 6, 0},
	{8, 0, 0, 0, 6, 0, 0, 0, 3}, {4, 0, 0, 8, 0, 3, 0, 0, 1}, {7, 0, 0, 0, 2, 0, 0, 0, 6},
	{0, 6, 0, 0, 0, 0, 2, 8, 0}, {0, 0, 0, 4, 1, 9, 0, 0, 5}, {0, 0, 0, 0, 8, 0, 0, 7, 9},
}

var poznateVrijednosti = make(chan int)

func main() {

	/*for x := 0; x < 9; x++ {
	    for y := 0; y < 9; y++ {
	        fmt.Println("X =", x, "Y =", y, "Vrijednost na toj poziciji je:", m[x][y], "\n")
	    }
	}*/

	fmt.Printf("\n")
	printBoard(m)

	go_rows(m)
	time.Sleep(1000 * time.Millisecond)

	/*for i := 0; i < cap(poznateVrijednosti)+1; i++ {
		val := <-poznateVrijednosti
		fmt.Printf("Vrijednosti iz kanala: %v\n", val)
	}*/
}

func printBoard(board matrix) {
	fmt.Printf("=====================\n")
	for y := 0; y < 9; y++ {
		for x := 0; x < 9; x++ {
			fmt.Printf("%d ", board[y][x])
			if x%3 == 2 && x < 8 {
				fmt.Printf("| ")
			}
		}
		fmt.Printf("\n")
		if y%3 == 2 && y < 8 {
			fmt.Printf("-----   -----   -----\n")
		}
	}
	fmt.Printf("=====================\n\n")
}

func go_rows(rows matrix) {
	for i := 0; i < 9; i++ {
		//fmt.Printf("Iteration: %v ->", i)
		//fmt.Printf("%v\n", rows[i])
		go rijesi(rows[i], i)
		time.Sleep(50 * time.Millisecond)
	}
}

func rijesi(redak []int, x int) {
	for i := 0; i < 9; i++ {
		fmt.Printf("Vrijednost*** = %v, a broj retka %v\n", redak[i], x)
	}

	for j := 0; j < 9; j++ {
		fmt.Println("==========")
		fmt.Printf("Vrijednost iz matrice = %v, a broj retka %v\n", m[j][x], x)
	}

	//fmt.Printf("%v\n", redak)
	//fmt.Printf("Broj redka/stupca = %v\n", xy)
}

//fun rijesiDalje(poznateVrijednosti chan int, )
