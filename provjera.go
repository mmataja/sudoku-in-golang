package main

import "fmt"

type row []int
type matrix []row

func main() {
	m := matrix{
		{5, 3, 0, 0, 7, 0, 0, 0, 0}, {6, 0, 0, 1, 9, 5, 0, 0, 0}, {0, 9, 8, 0, 0, 0, 0, 6, 0},
		{8, 0, 0, 0, 6, 0, 0, 0, 3}, {4, 0, 0, 8, 0, 3, 0, 0, 1}, {7, 0, 0, 0, 2, 0, 0, 0, 6},
		{0, 6, 0, 0, 0, 0, 2, 8, 0}, {0, 0, 0, 4, 1, 9, 0, 0, 5}, {0, 0, 0, 0, 8, 0, 0, 7, 9},
	}

	fmt.Printf("%v", m[1][:3]) // za ovo treba for petlja

	fmt.Printf("\n")
	printBoard(m)

	go_rows(m)
	t := transpose(m)
	go_columns(t)

	fmt.Printf("%v", t[1])

}

func transpose(m matrix) matrix {
	r := make(matrix, len(m[0]))
	for x, _ := range r {
		r[x] = make(row, len(m))
	}
	for y, s := range m {
		for x, e := range s {
			r[x][y] = e
		}
	}
	return r
}

func printBoard(board matrix) {
	fmt.Printf("=====================\n")
	for y := 0; y < 9; y++ {
		for x := 0; x < 9; x++ {
			fmt.Printf("%d ", board[y][x])
			if x%3 == 2 && x < 8 {
				fmt.Printf("| ")
			}
		}
		fmt.Printf("\n")
		if y%3 == 2 && y < 8 {
			fmt.Printf("-----   -----   -----\n")
		}
	}
	fmt.Printf("=====================\n\n")
}

func go_rows(board matrix) {
	for i := 1; i <= 9; i++ {
		fmt.Printf("%v\n", board[:i])
	}
}

func go_columns(board matrix) {
	for i := 1; i <= 9; i++ {
		fmt.Printf("%v\n", board[:i])
	}
}
