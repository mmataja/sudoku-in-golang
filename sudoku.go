package main

import "fmt"

var sudoku = [9][9]int{
	{5, 3, 0, 0, 7, 0, 0, 0, 0}, {6, 0, 0, 1, 9, 5, 0, 0, 0}, {0, 9, 8, 0, 0, 0, 0, 6, 0},
	{8, 0, 0, 0, 6, 0, 0, 0, 3}, {4, 0, 0, 8, 0, 3, 0, 0, 1}, {7, 0, 0, 0, 2, 0, 0, 0, 6},
	{0, 6, 0, 0, 0, 0, 2, 8, 0}, {0, 0, 0, 4, 1, 9, 0, 0, 5}, {0, 0, 0, 0, 8, 0, 0, 7, 9},
}

type koordinate struct{ x, y, v int }

type Mozak struct {
	kanalSalji_dalje chan koordinate
	kanalEliminiraj  [9][9]chan int
	koordinate       [9][9]int
}

func main() {
	fmt.Println("\n")
	printSudoku(sudoku)

	var got = NoviMozak()
	got.spremiPoznatuVrijednosti()

	gotovo := got.rijesi()
	printSudoku(gotovo)
}

// Funkcija za printanje Sudoku board-a
func printSudoku(board [9][9]int) {
	fmt.Printf("=====================\n")
	for y := 0; y < 9; y++ {
		for x := 0; x < 9; x++ {
			fmt.Printf("%d ", board[y][x])
			if x%3 == 2 && x < 8 {
				fmt.Printf("| ")
			}
		}
		fmt.Printf("\n")
		if y%3 == 2 && y < 8 {
			fmt.Printf("-----   -----   -----\n")
		}
	}
	fmt.Printf("=====================\n\n")
}

// Initializes the Mozak structure with appropriate channels and go-routines.
func NoviMozak() (s *Mozak) {
	s = &Mozak{kanalSalji_dalje: make(chan koordinate)}
	for y := 0; y < 9; y++ {
		for x := 0; x < 9; x++ {
			s.kanalEliminiraj[y][x] = make(chan int)
			go polje(x, y, s.kanalEliminiraj[y][x], s.kanalSalji_dalje)
		}
	}
	return
}

// funkcija spremiPoznatuVrijednosti puni sudoku ploču sa poznatim vrijednostima
func (s *Mozak) spremiPoznatuVrijednosti() {
	for y := 0; y < 9; y++ {
		for x := 0; x < 9; x++ {
			s.posaljiPoznatuVrijednosti(x, y, sudoku[y][x])
		}
	}
}

// funckija posaljiPoznatuVrijednost salje dakle poznate vrijednosti u odgovarajući kanal "kanalEliminiraj"
func (s *Mozak) posaljiPoznatuVrijednosti(x, y, v int) {
	go func() {
		for i := 1; i <= 9; i++ {
			if i != v && v != 0 {
				s.kanalEliminiraj[y][x] <- i
			}
		}
	}()
}

// svaki kvadrat ("kockica") ima svoju go-routinu. Ona određuje kakvu vrijednost ima
// tako da gleda koja vrijednost ne može biti, i ako vidi 8 takvih vrijednosti,
// zna da onda ona mora biti preostala deveta vrijednost.
// Potom šalje odgovor kanalu "kanalSalji_dalje".
func polje(x, y int, kanalEliminiraj <-chan int, kanalSalji_dalje chan<- koordinate) {
	var eliminirani_brojevi [9]bool
	for n := range kanalEliminiraj {
		eliminirani_brojevi[n-1] = true
		var c, s int
		for i, v := range eliminirani_brojevi {
			if v {
				c++
			} else {
				s = i + 1
			}
		}
		if c == 8 {
			kanalSalji_dalje <- koordinate{x, y, s}
			for _ = range kanalEliminiraj {
			}
			return
		}
	}
}

// Funkcija rijesi iterira kroz sva polja kanala "kanalSalji_dalje" te šalje određene vrijednosti u
// funkciju eliminiraj kako bi ona kreirala nova ograničenja.
func (s *Mozak) rijesi() [9][9]int {
	for n := 0; n < 9*9; n++ {
		u := <-s.kanalSalji_dalje
		go s.eliminiraj(u)
		s.koordinate[u.y][u.x] = u.v

		// fmt.Printf("kanalSalji_dalje[%d]: %#v\n", n, u)
		// printSudoku(s.koordinate)
	}
	return s.koordinate
}

// Funkcija eliminiraj šalje sve vrijednosti u odgovarajuće redove, stupce i 3x3 box
// kako bi polja imala nove informacije za određivanje novih ograničenja
func (s *Mozak) eliminiraj(u koordinate) {
	// red
	for x := 0; x < 9; x++ {
		if x != u.x && u.v != 0 {
			s.kanalEliminiraj[u.y][x] <- u.v
		}
	}
	// stupac
	for y := 0; y < 9; y++ {
		if y != u.y && u.v != 0 {
			s.kanalEliminiraj[y][u.x] <- u.v
		}
	}
	// 3x3 box
	sX, sY := u.x/3*3, u.y/3*3
	for y := sY; y < sY+3; y++ {
		for x := sX; x < sX+3; x++ {
			if x != u.x && y != u.y && u.v != 0 {
				s.kanalEliminiraj[y][x] <- u.v
			}
		}
	}
}
